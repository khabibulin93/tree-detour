# Программа для подсчета в txt файлах не пробельных символов

from pathlib import Path
import re

def letters_counter():
    for path in Path().rglob("*.txt"): # работа производится в папке в которой лежит скрипт
        text_file = open(path, "r").read()
        count_letters = len(re.findall('\S', text_file))  # поиск не пробельных значений в файлах
        print("%s \n\t Не пробельных символов: %s \n" % (path, count_letters))
if __name__ == '__main__':
    letters_counter()